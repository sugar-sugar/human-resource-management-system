$(document).ready(function(){
    //绑定按钮事件
    $('#btnLogin').bind('click',btnLoginClick);
    $('#errorMsg').hide();
});
function btnLoginClick(){
    $.ajax({
        url: 'http://localhost:63342/untitled/20201116/%E9%A1%B9%E7%9B%AE//data/user.json',
        method: 'get', //没有服务端支持
        dataType: 'json',
        //从json文件取数据
        success: function(data){
            var username = $('#username').val();
            var password = $('#password').val();
            var users = data.filter(function(item){
                return item.username == username && item.password == password;
            });
            if(users.length>0){
                $('#errorMsg').fadeOut();
                //实现页面跳转
                window.location.href='main.html';
            }else{
                $('#errorMsg').fadeIn();
            }
        }
    })
}